﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nop.Core.Domain.Customers;
using Nop.Services.Customers;
using Nop.Services.Security;

namespace Nop.Web.Controllers
{
    [Route("api/command")]
    [ApiController]
    public class CommandController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        private readonly IEncryptionService _encryptionService;

        private readonly CustomerSettings _customerSettings;

        public CommandController(
            ICustomerService customerService,
            IEncryptionService encryptionService,
            CustomerSettings customerSettings
            )
        {
            this._customerService = customerService;
            this._encryptionService = encryptionService;
            this._customerSettings = customerSettings;
        }

        [HttpPost]
        public IActionResult GetAllCustomer(CustomerAccountAndPassword customerAccountAndPassword)
        {
            Customer customer = _customerService.GetCustomerByUsername(customerAccountAndPassword.UsernameAccountCode);

            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                if (customer != null)
                {
                    CustomerPassword customerPassword = _customerService.GetCurrentPassword(customer.Id);

                    var saltKey = _encryptionService.CreateSaltKey(NopCustomerServicesDefaults.PasswordSaltKeySize);

                    customerPassword.PasswordSalt = saltKey;
                    customerPassword.Password = _encryptionService.CreatePasswordHash(customerAccountAndPassword.Password, saltKey, _customerSettings.HashedPasswordFormat);
                    customerPassword.PasswordFormatId = 1;
                    _customerService.UpdateCustomerPassword(customerPassword);
                }
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }
    }
}