﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// 從蝴蝶來的帳密
    /// </summary>
    public class CustomerAccountAndPassword
    {
        public string UsernameAccountCode { get; set; }

        public string Password { get; set; }
    }
}
